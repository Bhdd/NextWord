﻿namespace Domain
{
    using System;
    using System.Web;
    using MSharp.Framework;

    /// <summary>
    /// Provides the functionality to reset a user's password.
    /// </summary>
    public class PasswordResetService
    {
        User User;
        PasswordResetTicket Ticket;

        PasswordResetService(User user) { User = user; }

        /// <summary>
        /// Creates a new Password Reset Ticket for the specified user.
        /// </summary>
        public static void RequestTicket(User user)
        {
            var service = new PasswordResetService(user);

            using (var scope = Database.CreateTransactionScope())
            {
                service.CreateTicket();
                service.SendEmail();
                scope.Complete();
            }
        }

        /// <summary>
        /// Completes the password recovery process.
        /// </summary>
        public static void Complete(PasswordResetTicket ticket, string newPassword)
        {
            if (newPassword.IsEmpty()) throw new ArgumentNullException(nameof(newPassword));

            if (ticket.IsExpired)
                throw new ValidationException("This ticket has expired. Please request a new ticket.");

            if (ticket.IsUsed) throw new ValidationException("This ticket has been used once. Please request a new ticket.");

            var service = new PasswordResetService(ticket.User);

            using (var scope = Database.CreateTransactionScope())
            {
                service.UpdatePassword(newPassword);
                Database.Update(ticket, t => t.IsUsed = true);

                scope.Complete();
            }
        }

        void CreateTicket()
        {
            Database.Save(Ticket = new PasswordResetTicket { User = User });
        }

        void SendEmail()
        {
            EmailTemplate.RecoverPassword.Send(User, new
            {
                UserId = User.Name,
                Link = $"<a href='{GetResetPasswordUrl()}'> Reset Your Password </a>",
            });
        }

        string GetResetPasswordUrl()
        {
            return HttpContext.Current.Request.GetAbsoluteUrl("/password/reset/" + Ticket.ID);
        }

        void UpdatePassword(string newPassword)
        {
            Database.Update(User, u => u.Password = newPassword.Trim().CreateHash(u.Salt));
        }
    }
}
