﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Domain
{
    public static class Utils
    {
        //public static Array Delays = new byte[] { 1, 3, 7, 14, 30, 60, 90 };
        public static readonly List<string> Times = new List<string>(new string[]
   {
       "Select", "8:00 am","9:00 am","10:00 am","11:00 am","12:00 am","1:00 pm","2:00 pm","3:00 pm",
        "4:00 pm","5:00 pm","6:00 pm","7:00 pm", "8:00 pm"

   });
        public static int[] Delays = new int[7] { 1, 3, 7, 14, 30, 60, 90 };




        public enum WordType { Word = 1, Phrase };



        public static int GenerateRandomNumber()
        {
            int _min = 100000;
            int _max = 999999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }
    }
}