﻿namespace Domain
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Collections.Generic;
    using System.Text;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Services;

    /// <summary>
    /// Provides the business logic for Learning class.
    /// </summary>
    partial class Learning
    {
        public static IEnumerable<Learning> GetListByUserID(Guid userId) => Database.GetList<Learning>(x => x.UserId == userId);
    }
}