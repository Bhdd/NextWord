﻿namespace Domain
{
    using System;
    using MSharp.Framework;

    partial class LogonFailure
    {
        const int ATTEMPTS_BEFORE_CAPTCHA = 3;

        public static bool NextAttemptNeedsCaptcha(string email, string ip)
        {
            var attempts = 1 + Database.Find<LogonFailure>(l => l.IP == ip && l.Email == email)?.Attempts ?? 0;

            return attempts >= ATTEMPTS_BEFORE_CAPTCHA;
        }

        public static bool MustShowCaptcha(string email, string ip)
        {
            var attempts = 1 + Database.Find<LogonFailure>(l => l.IP == ip && l.Email == email)?.Attempts ?? 0;

            RecordAttempt(email, ip, attempts);

            return attempts > ATTEMPTS_BEFORE_CAPTCHA;
        }

        public static void Remove(string email, string ip)
        {
            Database.DeleteAll<LogonFailure>(x => x.Email == email || x.IP == ip);
        }



        static void RecordAttempt(string email, string ip, int attempts)
        {
            var attempt = Database.Find<LogonFailure>(l => l.IP == ip || l.Email == email)?.Clone() ?? new LogonFailure();

            attempt.Email = email;
            attempt.IP = ip;
            attempt.Attempts = attempts;
            attempt.Date = LocalTime.Now;

            Database.Save(attempt);
        }
    }
}