﻿namespace Domain
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Collections.Generic;
    using System.Text;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Services;
    using System.Xml.Linq;

    /// <summary>
    /// Provides the business logic for Word class.
    /// </summary>
    partial class Word
    {
        public static string GetMeaning(string wordText)
        {
            try
            {
                var path = System.Web.Hosting.HostingEnvironment.MapPath("~/public/xml/" + wordText + ".xml");
                var doc = XDocument.Load(path);
                var word = doc.GetElement("page/title").Value;
                var meanings = doc.Descendants("meaning").Select(m => m.Value);
                return string.Join(Environment.NewLine, meanings);
            }
            catch (System.IO.FileNotFoundException)
            {
                return "Xml file not found";
            }
        }
    }
}