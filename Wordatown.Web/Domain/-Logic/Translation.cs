﻿namespace Domain
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Collections.Generic;
    using System.Text;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Services;
    using System.Xml.Linq;
    using System.IO;

    /// <summary>
    /// Provides the business logic for Translation class.
    /// </summary>
    partial class Translation
    {
        public static IEnumerable<Translation> GetTranslations()
        {
            var trans = new List<Translation>();
            var path = System.Web.Hosting.HostingEnvironment.MapPath("~/public/xml/");

            foreach (var file in Directory.EnumerateFiles(path))
            {
                var doc = XDocument.Load(file);
                var word = doc.GetElement("page/title").Value;
                var transNode = doc.GetElement("page/translations");

                if (transNode == null)
                    continue;

                foreach (var t in transNode.Descendants())
                {
                    trans.Add(new Translation
                    {
                        Language = Database.Find<Language>(x => x.Name == t.Name.ToString()),
                        Word = Database.Find<Word>(x => x.Text == word),
                        TranslationText = t.Value
                    });
                }
            }

            return trans;
        }
    }
}