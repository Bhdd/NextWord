namespace Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;
    using MSharp.Framework.Services;

    partial class User : IUser, IPrincipal, IIdentity
    {
        /// <summary>
        /// Gets the roles of this user.
        /// </summary>
        public virtual IEnumerable<string> GetRoles()
        {
            if (System.Web.HttpContext.Current.Request.IsLocal) yield return "Local.Request";

            yield return "User";
        }

        /// <summary>
        /// Invoked just before this object is validated.
        /// </summary>
        protected override void OnValidating(EventArgs e)
        {
            base.OnValidating(e);

            if (Salt.IsEmpty()) Salt = Guid.NewGuid().ToString();
        }

        #region IPrincipal

        IIdentity IPrincipal.Identity => this;

        /// <summary>
        /// Specifies whether or not this user has a specified role.
        /// </summary>
        public bool IsInRole(string role) => GetRoles().Contains(role);

        string IIdentity.AuthenticationType => "ApplicationAuthentication";

        bool IIdentity.IsAuthenticated => true;

        string IIdentity.Name => ID.ToString();

        #endregion
    }
}