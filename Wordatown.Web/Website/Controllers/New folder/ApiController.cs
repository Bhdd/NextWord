﻿namespace MSharp.Framework.Api
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using Framework.Services;

    public class ApiController : System.Web.Http.ApiController
    {
        public NotFoundTextActionResult NotFound(string message) => new NotFoundTextActionResult(message, Request);

        public UnauthorizedTextActionResult Unauthorized(string message)
        {
            return new UnauthorizedTextActionResult(message, Request);
        }

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);

            if (User?.IsInRole("Anonymous") ?? true)
                JwtAuthenticate();
        }

        /// <summary>
        /// Dispatches the specified file.
        /// </summary>
        protected HttpResponseMessage File(FileInfo file)
        {
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(file.ReadAllBytes())
            };

            result.Content.Headers.Perform(h =>
            {
                h.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = file.Name };
                h.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            });

            return result;
        }

        protected void JwtAuthenticate()
        {
            var user = JwtAuthentication.ExtractUser(Request.Headers);

            if (user != null && !(user is IPrincipal))
                throw new Exception("User should implement IPrincipal.");

            HttpContext.Current.User = User = user as IPrincipal;
        }



    }
}