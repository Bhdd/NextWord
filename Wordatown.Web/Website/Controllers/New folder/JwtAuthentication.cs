﻿namespace MSharp.Framework.Api
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http.Headers;
    using System.Security.Principal;
    using Framework;
    using JWT;
    using MSharp.Framework.Services;

    public class JwtAuthentication
    {
        public static string CreateTicket(IUser user, DateTime? expiryDate = null)
        {
            var token = new { ID = user.GetId(), Expiry = expiryDate?.ToString() };

            return JsonWebToken.Encode(token, Config.Get("JWT.Token.Secret"), JwtHashAlgorithm.HS256);
        }

        public static string ExtractUserId(HttpRequestHeaders headers)
        {
            if (headers.Authorization?.Scheme != "Bearer") return null;

            try
            {
                var jwt = headers.Authorization.Parameter;

                if (jwt.IsEmpty()) return null;

                var values = JsonWebToken.DecodeToObject(jwt, Config.Get("JWT.Token.Secret"), verify: true) as IDictionary<string, object>;

                var expiry = values["Expiry"].ToStringOrEmpty().TryParseAs<DateTime>();

                if (expiry <= LocalTime.Now) return null;

                return values["ID"] as string;
            }
            catch (ArgumentException)
            {
                return null; // invalid JWT format
            }
            catch (SignatureVerificationException)
            {
                return null;
            }
        }

        public static IUser ExtractUser(HttpRequestHeaders headers)
        {
            var userId = ExtractUserId(headers);

            if (userId.IsEmpty()) return null;
            //var l = Database.GetList<IUser>();
            return Database.GetOrDefault<Domain.User>(userId);
        }
    }
}