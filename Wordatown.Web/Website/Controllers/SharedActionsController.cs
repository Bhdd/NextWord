﻿namespace Controllers
{
    using System;
    using System.ComponentModel;
    using System.Web.Mvc;
    using MSharp.Framework;
    using MSharp.Framework.Mvc;
    public class SharedActionsController : BaseController
    {
        [Route("error")]
        public ActionResult Error()
        {
            return View("error");
        }

        [Route("error/404")]
        public ViewResult NotFound()
        {
            return View("error-404");
        }

        [HttpPost, Route("file/upload")]
        [Authorize]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public ActionResult UploadTempFileToServer()
        {
            // Note: This will prevent uploading of all unsafe files defined at MSharp.Framework.Document.UnsafeExtensions
            // If you need to allow them, then comment it out.
            if (Document.HasUnsafeFileExtension(Request.Files[0].FileName))
                return Json(new { Error = "Invalid file extension." });

            var file = Request.Files[0];
            var path = System.IO.Path.Combine(FileUploadService.GetFolder(Guid.NewGuid().ToString()).FullName, file.FileName.ToSafeFileName());
            if (path.Length >= 260)
                return Json(new { Error = "File name length is too long." });

            return Json(new FileUploadService().TempSaveUploadedFile(Request));
        }

        [Route("file/download")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public ActionResult DownloadFile()
        {
            var path = Request.Url.Query.TrimStart('?');
            var accessor = new FileAccessor(path, User);
            if (!accessor.IsAllowed()) return new HttpUnauthorizedResult(accessor.SecurityErrors);

            if (accessor.Document.IsMedia()) return new RangeFileContentResult(accessor.Document);
            else return File(accessor.Document);
        }

        [Route("temp-file/{key}")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public ActionResult DownloadTempFile(string key)
        {
            return TempFileService.Download(key);
        }
    }
}

