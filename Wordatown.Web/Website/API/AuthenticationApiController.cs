﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.Http;
using Domain;
using MSharp.Framework;
using MSharp.Framework.Api;

namespace Controllers
{
    [RoutePrefix("api/v1/authentication")]
    public class AuthenticationApiController : MSharp.Framework.Api.ApiController
    {
        const string DEACTIVATED_USER_ERROR_MESSAGE = "Your account is currently deactivated. It might be due to security concerns on your account. Please contact the system administrator to resolve this issue. We apologise for the inconvenience.";
        const string DUPLICATE_EMAIL_ERROR_MESSAGE = "Provided email id already exist for another user.";
        const string EMAIL_PASSWORD_MANDATORY_ERROR_MESSAGE = "Email should be provided.";

        public struct LoginArgs { public string Email, Password, DeviceType, DeviceModel; public Guid? LanguageId; }
        [HttpPost, Route("login")]
        public IHttpActionResult Login(LoginArgs args)
        {
            if (args.Email.TrimOrEmpty().IsEmpty() || args.Password.TrimOrEmpty().IsEmpty())
                return BadRequest(EMAIL_PASSWORD_MANDATORY_ERROR_MESSAGE);

            var user = Database.Find<User>(u => u.Email == args.Email);
            if (user != null && user.Password != args.Password.CreateHash(user.Salt)) user = null;

            if (user == null) return BadRequest("Invalid email or password.");

            if (user.IsDeactivated) return BadRequest(DEACTIVATED_USER_ERROR_MESSAGE);

            Database.Update(user, u => u.NativeLanguageId = args.LanguageId);

            return Ok(DoLogin(args, user));
        }

        string DoLogin(LoginArgs args, User user)
        {
            var token = JwtAuthentication.CreateTicket(user);

            //var deviceLogin = Database.Find<UserDeviceLogin>(d => d.SessionToken == token);
            //if (deviceLogin != null)
            //    Database.Update(deviceLogin, i => i.LogoutDateTime = DateTime.Now);

            //Database.Save(new UserDeviceLogin
            //{
            //    User = user,
            //    LoginDateTime = DateTime.Now,
            //    DeviceType = args.DeviceType,
            //    DeviceModel = args.DeviceModel,
            //    SessionToken = token,
            //});

            return token;
        }

        [HttpGet, Route("user")]
        public IHttpActionResult GetCurrentUser()
        {
            if (User == null) return BadRequest("Current user is null.");
            return Ok(User);
        }

        public struct RegisterArgs { public string Email, DeviceType, DeviceModel; public Guid? LanguageId; }
        [HttpPost, Route("register")]
        public IHttpActionResult Register(RegisterArgs model)
        {
            if (model.Email.IsEmpty()) return BadRequest(EMAIL_PASSWORD_MANDATORY_ERROR_MESSAGE);

            var user = Domain.User.FindByEmail(model.Email);
            if (user != null) return Ok(true); //BadRequest(DUPLICATE_EMAIL_ERROR_MESSAGE);

            var pin = Utils.GenerateRandomNumber().ToString();
            var salt = Guid.NewGuid().ToString();
            var hashedPassword = pin.CreateHash(salt);

            try
            {
                using (var scope = Database.CreateTransactionScope())
                {
                    user = Database.Save(new User
                    {
                        Email = model.Email,
                        Password = hashedPassword,
                        Salt = salt,
                        NativeLanguageId = model.LanguageId
                    });

                    EmailTemplate.UserRegistration.Send(user, new
                    {
                        Email = user.Email,
                        Pin = pin,
                    });

                    var defaultReminders = new List<Reminder>
                    {
                        new Reminder{ Time = new TimeSpan(8,0,0), User = user},
                        new Reminder{ Time = new TimeSpan(20,0,0), User = user}
                    };

                    Database.Save(defaultReminders);

                    scope.Complete();
                }

                return Ok(true);
            }
            catch
            {
                return BadRequest("An error occured during registration. Please try again");
            }
        }
    }
}
