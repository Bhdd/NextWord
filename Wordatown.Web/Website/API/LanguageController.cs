﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using MSharp.Framework;

namespace Controllers.Api
{
    [RoutePrefix("api/v1/Language")]
    public class LanguageController : BaseApiController
    {
        [HttpGet, Route("")]
        public IHttpActionResult GetLanguages()
        {
            var result = Database.GetList<Domain.Language>();
            return Ok(result);
        }
    }
}