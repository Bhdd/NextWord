using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Http;
using Domain;
using MSharp.Framework;

namespace Controllers
{
    [Authorize]

    public class UserAssessmentController : BaseApiController
    {
        //[HttpGet]
        //[Route("api/UserAssessment/GetUserAssessmentList/")]
        //public string[] GetUserAssessmentList(int page)
        //{
        //    var userAssessment = Database.GetList<UserAssessment>(x => x.UserId == User.ID).Select(x => x);
        //    var dbWords = Database.GetList<Word>(x => x.WordType == 1).ToList().OrderBy("Rank").Select(x => x.WordText).Skip((page - 1) * 1000).Take(1000).ToList();
        //    var query =
        //   from word in dbWords
        //   where !(userAssessment.Select(y => y.Word).Contains(word)) ||
        //   (userAssessment.Where(y => y.Known != true).Select(y => y.Word).Contains(word))
        //   select word;
        //    var words = dbWords.Skip(new Random().Next(0, 980)).Take(20).ToArray();
        //    return words;
        //}
        //[HttpGet]
        //[Route("api/UserAssessment/GetGapMapData")]
        //public int[,] GetGapMapData()
        //{
        //    IEnumerable<Word> dbWords = Database.GetList<Word>(x => x.WordType == 1, MSharp.Framework.Data.QueryOption.OrderBy("Rank"));
        //    IEnumerable<UserAssessment> dbUserAssessment = Database.GetList<UserAssessment>(x => x.UserId == this.User).Select(x => x).ToArray();
        //    var dbLearning = Database.GetList<Learning>(x => x.UserId == this.User).Select(x => x).ToArray();
        //    int row = 1;
        //    int[,] stat = new int[12, 3];
        //    while (row <= 12)
        //    {
        //        IEnumerable<Word> Words = dbWords.Skip((row - 1) * 1000).Take(1000);
        //        int NA =
        //        (from w in (from word in Words
        //                    where (dbUserAssessment.Where(x => x.Known == null).Select(x => x.Word.ToLower()).Contains(word.WordText.ToLower()))
        //                    select word)
        //         where !(dbLearning.Where(y => y.IsKnown != null).Select(y => y.WordId).Contains(w.ID))
        //         select w)
        //        .Count();

        //        int Known =
        //        (from w in (from word in Words
        //                    where (dbUserAssessment.Where(x => x.Known == true).Select(x => x.Word.ToLower()).Contains(word.WordText.ToLower()))
        //                    select word)
        //         where !(dbLearning.Where(y => y.IsKnown == false).Select(y => y.WordId).Contains(w.ID))
        //         select w)
        //        .Count();

        //        int unKnown =
        //        (from w in (from word in Words
        //                    where (dbUserAssessment.Where(x => x.Known == false).Select(x => x.Word.ToLower()).Contains(word.WordText.ToLower()))
        //                    select word)
        //         where !(dbLearning.Where(y => y.IsKnown == true).Select(y => y.WordId).Contains(w.ID))
        //         select w)
        //        .Count();

        //        stat[row - 1, 0] = NA * 50;
        //        stat[row - 1, 1] = Known * 50;
        //        stat[row - 1, 2] = unKnown * 50;
        //        row++;
        //    }
        //    return stat;
        //}
        //[HttpPut]
        //[Route("api/UserAssessment/ToggleKnown")]
        //public IHttpActionResult ToggleKnown(string word, bool? known)
        //{
        //    try
        //    {
        //        var userA = new UserAssessment();
        //        var userAssess = Database.Find<UserAssessment>(x => (x.Word == word && x.User == User.ID));
        //        if (userAssess != null)
        //        {
        //            userA = userAssess.Clone();
        //        }
        //        else
        //        {
        //            userA.User = User;
        //            userA.Word = word;
        //        }
        //        Learning Learning = new Learning();
        //        var myWord = Database.Find<Word>(x => x.WordText.ToLower().Trim() == word.ToLower().Trim());
        //        if (word.HasValue())
        //        {
        //            Learning = Database.Find<Learning>(x => x.UserId == User.ID && x.WordId == myWord.ID);
        //            if (Learning != null)
        //            {
        //                Learning.IsKnown = known;
        //            }
        //            else
        //            {
        //                Learning = new Learning();
        //                Learning.IsKnown = known;
        //                Learning.UserId = User.ID;
        //                Learning.WordId = myWord.ID;

        //            }
        //        }
        //        var ul = Learning.Clone();
        //        userA.Known = known;
        //        Database.Save(userA);
        //        Database.Save(ul);
        //    }
        //    catch (DBConcurrencyException)
        //    {
        //        throw;
        //    }
        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //[HttpDelete]
        //[Route("api/UserAssessment/RemoveAll")]
        //public IHttpActionResult RemoveAll()
        //{
        //    try
        //    {
        //        Database.DeleteAll<UserAssessment>(x => x.UserId == User.ID);

        //    }
        //    catch (DBConcurrencyException)
        //    {
        //        throw;
        //    }
        //    return StatusCode(HttpStatusCode.NoContent);
        //}

    }
}