﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using Domain;
using MSharp.Framework;

namespace Controllers
{
    [Authorize]
    [RoutePrefix("api/v1/learning")]
    public class LearningController : BaseApiController
    {
        [HttpPut, Route("{id}/notsure")]
        public IHttpActionResult MarkWordAsNotSure(Guid id)
        {
            try
            {
                var Learning = Database.Find<Learning>(x => x.ID == id);
                Database.Update(Learning, x =>
                {
                    x.DateLastShown = DateTime.Now;
                    x.TotalIsKnowns = 0;
                    x.Delay = 1;
                    x.TotalNotSures += 1;
                    x.DateAdded = DateTime.Now;
                });

                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut, Route("{id}/skip")]
        public IHttpActionResult SkipWord(Guid id)
        {
            try
            {
                var Learning = Database.Find<Learning>(x => x.ID == id);
                Database.Update(Learning, x =>
                {
                    x.DateLastShown = DateTime.Now;
                    x.TotalIsKnowns += 1;
                    x.IsKnown = x.TotalIsKnowns >= 7;
                    x.TotalSkips += 1;
                    x.Delay = Utils.Delays[Utils.Delays.IndexOf(Learning.Delay) + 1];
                    x.DateAdded = DateTime.Today;
                });

                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut, Route("{id}/known")]
        public IHttpActionResult ToggleWordAsKnown(Guid id)
        {
            try
            {
                var Learning = Database.Find<Learning>(u => u.UserId == User.ID && u.WordId == id);
                if (Learning == null)
                {
                    Database.Save(new Learning
                    {
                        WordId = id,
                        UserId = User.ID,
                        IsKnown = true
                    });
                }
                else Database.Update(Learning, x => x.IsKnown = !x.IsKnown);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        [HttpPut, Route("{id}/tolearn")]
        public IHttpActionResult ToggleWordAsToLearn(Guid id)
        {
            try
            {
                var Learning = Database.Find<Learning>(u => u.UserId == User.ID && u.WordId == id);
                if (Learning == null)
                {
                    Database.Save(new Learning
                    {
                        WordId = id,
                        UserId = User.ID,
                        IsKnown = false,
                        DateAdded = DateTime.Now,
                        Delay = 1,
                        TotalNotSures = 0,
                        TotalSkips = 0
                    });
                }
                else Database.Delete(Learning);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        [HttpPut, Route("{rank}/markwordsbeforeasknown/{isphrase}")]
        public IHttpActionResult MarkWordsBeforeAsKnown(int rank, bool isphrase)
        {
            var listToSave = new List<Learning>();

            var allWords = Database.GetList<Word>(x => x.IsPhrase == isphrase && x.Rank < rank).OrderBy(x => x.Rank);
            var allLearningsWords = Database.GetList<Learning>(x => x.UserId == User).Select(l => l.WordId);
            var words = allWords.Where(x => !allLearningsWords.Contains(x.ID));

            foreach (var word in words)
            {
                listToSave.Add(new Learning
                {
                    WordId = word.ID,
                    UserId = User.ID,
                    IsKnown = true,
                    Delay = 1
                });
            }

            try
            {
                Database.Save(listToSave);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        [HttpGet, Route("reminders")]
        public IHttpActionResult GetReminders()
        {
            var reminders = Database.GetList<Reminder>(u => u.User == User);
            return Ok(reminders);
        }

        public class SettingsData { public IEnumerable<TimeSpan> Reminders; public Guid LanguageId; }
        [HttpPost, Route("settings")]
        public IHttpActionResult SaveSettings(SettingsData data)
        {
            try
            {
                Database.DeleteAll<Reminder>(u => u.User == User);
                Database.Save(data.Reminders.Select(r => new Reminder { User = User, Time = r }));
                Database.Update(User, u => u.NativeLanguageId = data.LanguageId);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok(true);
        }
    }
}