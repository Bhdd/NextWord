﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Xml.Linq;
using Domain;
using MSharp.Framework;
using System.Text.RegularExpressions;
using System.Web.Hosting;

namespace Controllers
{
    [Authorize]
    [RoutePrefix("api/v1/words")]
    public class WordsController : BaseApiController
    {
        [HttpGet, Route("learningqueue")]
        public IHttpActionResult GetWordsInLearningQueue()
        {
            return Ok(Database.GetList<Learning>(
                c => c.IsKnown == false &&
                c.IsLearnt != true &&
                c.UserId == User.ID
                && (DateTime.Now.Date >= (c.DateLastShown.GetValueOrDefault().AddDays(c.Delay)) || c.Delay == 1))
                .OrderBy(l => l.DateAdded));
        }

        [HttpGet, Route("pending/{isphrase:bool}")]
        public IHttpActionResult GetPendingWords(bool isphrase)
        {
            var allWords = Database.GetList<Word>(x => x.IsPhrase == isphrase);
            var wordsUserIsAlreadyLearning = Database.GetList<Learning>(x => x.UserId == User).Select(x => x.WordId);
            return Ok(allWords.Where(w => wordsUserIsAlreadyLearning.Lacks(w.ID)).OrderBy(w => w.Rank).Take(200));
        }

        [HttpGet, Route("known/{isphrase:bool}")]
        public IHttpActionResult GetKnownWords(bool isphrase)
        {
            var knownWords = Database.GetList<Learning>(
                x => x.User == User &&
                x.IsKnown == true &&
                x.Word.IsPhrase == isphrase).OrderByDescending(x => x.DateLastShown).Select(x => x.Word);

            return Ok(knownWords);
        }

        [HttpGet, Route("tolearn/{isphrase:bool}")]
        public IHttpActionResult GetToLearnWords(bool isphrase)
        {
            var toLearnWords = Database.GetList<Learning>(
                x => x.User == User &&
                x.IsKnown == false &&
                x.IsLearnt == false &&
                x.Word.IsPhrase == isphrase).Select(x => x.Word);

            return Ok(toLearnWords);
        }

        [HttpGet, Route("knowncount")]
        public IHttpActionResult GetKnownWordsCount()
        {
            var knownCount = Database.Count<Learning>(x => x.User == User && x.IsKnown == true);
            var learntCount = Database.Count<Learning>(x => x.User == User && x.IsLearnt == true);
            return Ok(new { KnownCount = knownCount, LearntCount = learntCount });
        }

        [HttpGet, Route("learnt")]
        public IHttpActionResult GetLearntWords()
        {
            var list = Database.GetList<Learning>(c => c.IsLearnt == true && c.User == User);
            return Ok(list);

        }

        [HttpGet, Route("learning")]
        public IHttpActionResult GetLearningWords()
        {
            var list = Database.GetList<Learning>(c => c.IsKnown == false && c.IsLearnt != true && c.User == User);
            return Ok(list);
        }

        [HttpGet, Route("{word}/meaning")]
        public IHttpActionResult GetMeaning(string word)
        {
            var allMeanings = "";
            var translation = "";

            var path = HostingEnvironment.MapPath("~/public/xml/" + word.Replace(" ", string.Empty) + ".xml");
            var stringBuilder = new StringBuilder();
            try
            {
                //Get English meaning
                if (System.IO.File.Exists(path))
                {
                    var file = XDocument.Load(path);
                    var meanings = file.Descendants("meaning");
                    var regex = new Regex("[ ]{2,}", RegexOptions.None);

                    var index = 1;
                    foreach (var meaning in meanings)
                        stringBuilder.AppendLine(index++ + "- " + regex.Replace(meaning.Value, " "));

                    allMeanings = stringBuilder.ToString();

                    //Get Translation
                    var language = Database.Find<Language>(x => x.ID == User.NativeLanguageId);
                    if (language != null)
                    {
                        var translations = file.Descendants(XName.Get($"{language.Name}")).FirstOrDefault();
                        if (translations != null) translation = translations.Value;
                    }
                }
            }
            catch { }
            return Ok(new { Meaning = allMeanings, Translation = translation });
        }

        [HttpGet, Route("{word}/search")]
        public IHttpActionResult Search(string word)
        {
            var searchResults = Database.GetList<Word>(w => w.Text.ToLower().StartsWith(word.ToLower())).OrderBy(w => w.Text.Length);
            return Ok(searchResults);
        }

        //[AllowAnonymous]
        //[HttpGet]
        //[Route("api/Words/GetList")]
        //public IEnumerable<Word> GetWordsList()
        //{

        //    var path = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/xml/");
        //    foreach (var file in Directory.EnumerateFiles(path))
        //    {
        //        var doc = XDocument.Load(file);

        //        var word = doc.GetElement("page/title").Value;
        //        var meanings = doc.Descendants("meaning").Select(m => m.Value);
        //        yield return new Word
        //        {
        //            ID = Guid.NewGuid(),
        //            WordText = word,
        //            WordType = 1,
        //            Meaning = string.Join(Environment.NewLine, meanings),
        //        };
        //    }
        //}

        //[Route("api/Words/GetWordsStatistics")]
        //public int[,] GetWordsStatistics()
        //{
        //    IEnumerable<Word> dbWords = Database.GetList<Word>(x => x.WordType == 1, MSharp.Framework.Data.QueryOption.OrderBy("Rank"));
        //    IEnumerable<Learning> dbLearning = Database.GetList<Learning>(x => x.UserId == this.User).Select(x => x).ToArray();
        //    int row = 1;
        //    int[,] stat = new int[12, 3];
        //    while (row <= 12)
        //    {

        //        IEnumerable<Word> Words = dbWords.Skip((row - 1) * 1000).Take(1000);
        //        int NA =
        //         (from word in Words
        //          where !(dbLearning.Select(x => x.WordId).Contains(word.ID))
        //          select word).Count();
        //        int Known =
        //       (from word in Words
        //        where (dbLearning.Where(x => x.IsKnown == true).Select(x => x.WordId).Contains(word.ID))
        //        select word).Count();
        //        int unKnown =
        //        (from word in Words
        //         where (dbLearning.Where(x => x.IsKnown == false).Select(x => x.WordId).Contains(word.ID))
        //         select word).Count();
        //        stat[row - 1, 0] = NA;
        //        stat[row - 1, 1] = Known;
        //        stat[row - 1, 2] = unKnown;
        //        row++;
        //    }
        //    return stat;
        //}
    }
}