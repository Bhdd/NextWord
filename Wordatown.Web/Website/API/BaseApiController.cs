﻿namespace Controllers
{
    public class BaseApiController : MSharp.Framework.Api.ApiController
    {
        public new Domain.User User { get { return base.User as Domain.User; } }

    }
}
