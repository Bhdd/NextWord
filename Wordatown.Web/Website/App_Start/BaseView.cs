
    public abstract class BaseView<TModel> : MSharp.Framework.Mvc.WebViewPage<TModel>
{
    /// <summary>
    /// Gets the user security information for the current HTTP request.
    /// </summary>
    public new Domain.User User => base.User as Domain.User;
}