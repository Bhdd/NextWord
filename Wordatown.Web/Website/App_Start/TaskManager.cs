﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using MSharp.Framework;
using MSharp.Framework.Data;
using MSharp.Framework.Mvc;
using MSharp.Framework.Services;
using Domain;
using vm = ViewModel;

/// <summary>Executes the scheduled tasks in independent threads automatically.</summary>
public static partial class TaskManager
{
    private static List<AutomatedTask> tasks = new List<AutomatedTask>();
    
    public static IEnumerable<AutomatedTask> Tasks => tasks.ToArray();
    
    /// <summary>Initialize all automated tasks.</summary>
    static TaskManager()
    {
        tasks.Add(new AutomatedTask(CleanOldTempUploads)
        {
            Name = "Clean old temp uploads",
            Intervals = 10.Minutes()
        });
        
        tasks.Add(new AutomatedTask(SendEmailQueueItems)
        {
            Name = "Send email queue items",
            Intervals = 30.Seconds()
        });
    }
    
    /// <summary>
    /// This will start the scheduled activities.<para/>
    /// It should be called once in Application_Start global event.<para/>
    /// </summary>
    public static void Run() => Tasks.Do(t => t.Start());
    
    /// <summary>Clean old temp uploads</summary>
    static void CleanOldTempUploads(AutomatedTask info)
    {
        FileUploadService.DeleteTempFiles(olderThan: 1.Hours());
    }
    
    /// <summary>Send email queue items</summary>
    static void SendEmailQueueItems(AutomatedTask info)
    {
        EmailService.SendAll();
    }
}