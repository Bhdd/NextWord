﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using CaptchaMvc.Infrastructure;
using Domain;
using MSharp.Framework;
using System.Web.Http;

public class TheApplication : MSharp.Framework.Mvc.HttpApplication
{
    protected override void Application_Start()
    {
        GlobalConfiguration.Configure(config => config.MapHttpAttributeRoutes());

        base.Application_Start();

        ViewEngines.Engines.Add(new AppViewEngine());
        GlobalFilters.Filters.Add(new HandleApplicationErrorAttribute());
        RegisterEntityBindingParsers();
        CaptchaUtils.CaptchaManager.StorageProvider = new CookieStorageProvider();

        if (Config.Get<bool>("Automated.Tasks.Enabled"))
            TaskManager.Run();
    }

    void RegisterEntityBindingParsers()
    {
        // If you want to use the STRING format of any object in URL, then you can get MVC to bind the entity objects from their text automatically.

        // e.g. EntityModelBinder.RegisterParser<User>(routeValue => Domain.User.FindByName(routeValue));
    }

    protected override IPrincipal RetrieveActualUser(IPrincipal principal)
    {
        return base.RetrieveActualUser<User, AnonymousUser>(principal);
    }
}