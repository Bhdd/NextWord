﻿SET DATEFORMAT dmy
GO

INSERT [Users] (
[Id],
[Email],
[FirstName],
[IsDeactivated],
[LastName],
[NativeLanguage],
[Password],
[Salt]
)
VALUES (
N'0734677a-c81a-4d15-a679-f8c7eb8d6047'             -- Id
,N'b.partovi11@gmail.com'                           -- Email
,N'B'                                               -- FirstName
,0                                                  -- IsDeactivated
,N'P'                                               -- LastName
,NULL                                               -- NativeLanguage
,N'LBiMPuzUomMg4/Hoo+HwfkpTkFo'                     -- Password
,N'5a51f3e8-61ca-438a-85c5-737068760d33'            -- Salt
)

INSERT [Users] (
[Id],
[Email],
[FirstName],
[IsDeactivated],
[LastName],
[NativeLanguage],
[Password],
[Salt]
)
VALUES (
N'3536b2da-2e97-4627-bfc0-9036527b016b'             -- Id
,N'admin@uat.co'                                    -- Email
,N'Geeks'                                           -- FirstName
,0                                                  -- IsDeactivated
,N'Admin'                                           -- LastName
,NULL                                               -- NativeLanguage
,N'S/9g0WHI4Td8L2O2IE1POIgZV74'                     -- Password
,N'5a91f3e8-61ca-438a-85c5-737068760d33'            -- Salt
)

INSERT [Users] (
[Id],
[Email],
[FirstName],
[IsDeactivated],
[LastName],
[NativeLanguage],
[Password],
[Salt]
)
VALUES (
N'89001798-6ddd-46f3-9e94-44f3695820eb'             -- Id
,N'joe@uat.co'                                      -- Email
,N'Joe'                                             -- FirstName
,0                                                  -- IsDeactivated
,N'Admin'                                           -- LastName
,NULL                                               -- NativeLanguage
,N'S/9g0WHI4Td8L2O2IE1POIgZV74'                     -- Password
,N'5a51f3e8-61ca-438a-85c5-737068760d33'            -- Salt
)

GO
