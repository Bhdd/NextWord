﻿SET DATEFORMAT dmy
GO

INSERT [ContentBlocks] (
[Id],
[Content],
[Key]
)
VALUES (
N'1237271a-b526-4fef-81e4-075c18c3c1c2'             -- Id
,N'Your password has been successfully reset. '     -- Content
,N'PasswordSuccessfullyReset'                       -- Key
)

INSERT [ContentBlocks] (
[Id],
[Content],
[Key]
)
VALUES (
N'c66bda79-8da2-4d78-aa7f-f0a4dfd96641'             -- Id
,N'Welcome to our application. Please log in using the form below.' -- Content
,N'LoginIntro'                                      -- Key
)

GO
