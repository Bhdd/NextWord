-- Reminders Table ========================
CREATE TABLE [Reminders] (
    [Id] uniqueidentifier PRIMARY KEY NONCLUSTERED,
    [User] uniqueidentifier  NOT NULL,
    [Time] time  NOT NULL
)

