-- Learnings Table ========================
CREATE TABLE [Learnings] (
    [Id] uniqueidentifier PRIMARY KEY NONCLUSTERED,
    [Word] uniqueidentifier  NOT NULL,
    [User] uniqueidentifier  NOT NULL,
    [DateAdded] datetime2  NOT NULL,
    [IsKnown] bit  NOT NULL,
    [TotalSkips] int  NOT NULL,
    [TotalNotSures] int  NOT NULL,
    [TotalIsKnowns] int  NOT NULL,
    [Delay] int  NOT NULL,
    [DateLastShown] datetime2  NULL,
    [DateToBeShown] datetime2  NULL
)

