-- Users Table ========================
CREATE TABLE [Users] (
    [Id] uniqueidentifier PRIMARY KEY NONCLUSTERED,
    [FirstName] nvarchar(200)  NULL,
    [LastName] nvarchar(200)  NULL,
    [Email] nvarchar(100)  NOT NULL,
    [Password] nvarchar(100)  NULL,
    [Salt] nvarchar(200)  NOT NULL,
    [IsDeactivated] bit  NOT NULL,
    [NativeLanguage] uniqueidentifier  NULL
)

