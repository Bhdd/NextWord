ALTER TABLE [Reminders] ADD Constraint
                [FK_Remind.User]
                FOREIGN KEY ([User])
                REFERENCES [Users] ([ID])
                ON DELETE NO ACTION 
GO