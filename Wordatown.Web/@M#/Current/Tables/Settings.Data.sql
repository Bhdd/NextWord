﻿SET DATEFORMAT dmy
GO

INSERT [Settings] (
[Id],
[CacheVersion],
[Name],
[PasswordResetTicketExpiryMinutes]
)
VALUES (
N'f19f47bd-b41c-4c64-9d73-371e5f421830'             -- Id
,0                                                  -- CacheVersion
,N'Current'                                         -- Name
,120                                                -- PasswordResetTicketExpiryMinutes
)

GO
