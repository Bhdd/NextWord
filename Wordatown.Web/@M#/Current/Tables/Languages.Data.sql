﻿SET DATEFORMAT dmy
GO

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'01a353eb-9171-445b-aab4-1c0290897a0e'             -- Id
,N'Tamil'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'090c6fe5-c0a2-4cb9-8226-bbf5f3653cdb'             -- Id
,N'Bulgarian'                                       -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'0c9a13da-93bb-4bc4-a751-08e0dad35bbe'             -- Id
,N'Yoruba'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'0d6a914c-8b6e-4eed-b80e-f775d064cc24'             -- Id
,N'Telugu'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'0ff0b790-7775-4221-b086-517ca84637b7'             -- Id
,N'Ilocano'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'14948f52-7587-4403-ac36-5285b479606c'             -- Id
,N'Javanese'                                        -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'169a4d9e-a761-4c9f-a551-f03492169432'             -- Id
,N'Czech'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'16b38575-c42a-4c76-9b80-5a8a18a50a73'             -- Id
,N'Tagalog'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'1834a12b-9520-4857-8de5-808cec41bd54'             -- Id
,N'Haitian creole french'                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'1b5e9231-b62d-4390-8f6a-b666def67bea'             -- Id
,N'Russian'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'1cc85110-2953-4664-a3de-ffe7b7b481bd'             -- Id
,N'Lombard'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'1dc408f0-98dd-45ed-96fe-663df94081ea'             -- Id
,N'Madura'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'1f107d93-969a-43d8-ac46-74fa167cdeef'             -- Id
,N'Hiligaynon'                                      -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'20b3be79-89e7-4d4f-849b-42ae188c59b3'             -- Id
,N'Maithili'                                        -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'280655ad-2496-4be5-bb21-d085ddc05e1a'             -- Id
,N'Vietnamese'                                      -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'28852d8b-35d6-4fac-9c81-ca7238eedf64'             -- Id
,N'Uzbek'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'2fc4b73f-515b-49c3-9aa4-563ff4c1aea8'             -- Id
,N'Chittagonian'                                    -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'354a6ff1-7c51-494b-81ca-3aa7f8fd7542'             -- Id
,N'Kazakh'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'3587324c-28f9-4f11-8857-014bde8b284a'             -- Id
,N'Chhattisgarhi'                                   -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'36427335-b8f5-4183-a076-dfdd67f291b5'             -- Id
,N'Uyghur'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'38e041d6-2446-473d-a926-1ac64a0a4f7b'             -- Id
,N'Rwanda'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'39d0142d-7a5f-42b8-a551-81bd829c77f8'             -- Id
,N'Indonesian'                                      -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'39ede2a6-7419-4b1d-8116-ec71ca969024'             -- Id
,N'Deccan'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'3c1d5407-de39-4796-a015-65d17b5c139e'             -- Id
,N'Hungarian'                                       -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'3cc7db14-496e-497a-ad34-7b95c89492b1'             -- Id
,N'Khmer'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'3ecb18f3-ac46-4aaa-9ab9-7b9924d54927'             -- Id
,N'Tatar'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'4e119185-6a2d-4065-a894-dac3efeaa917'             -- Id
,N'Shona'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'5109f302-8549-4def-92bc-5bdbd7222a4b'             -- Id
,N'Somali'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'517911f7-d32f-40f5-bcbb-a3098a874ac6'             -- Id
,N'Oriya'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'52097cba-c09d-46b9-a256-9958dc2e0082'             -- Id
,N'Bhojpuri'                                        -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'547aa7b0-78dd-409b-9fd5-b8ea7effdc94'             -- Id
,N'Awadhi'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'59f12fe8-44f7-4471-8991-282c0b18001d'             -- Id
,N'Italian'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'5ff69680-a7eb-40b2-a32e-30b986ff03c6'             -- Id
,N'Oromo'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'623e129f-90e0-4b76-875c-f063ef881402'             -- Id
,N'Saraiki'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'630fb0f6-12fd-40fe-b933-1d633d3debed'             -- Id
,N'Sinhala'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'63c2def1-ba19-42b9-bcd0-3ff5e4782010'             -- Id
,N'Spanish'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'64fe71b6-c4e6-404e-805d-7e9c30c9bb13'             -- Id
,N'Hindi'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'699e8e2c-a3c6-4733-866c-4c49b8bb7609'             -- Id
,N'Zulu'                                            -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'7301b8d5-f803-4b1c-83d9-5e2e2420d323'             -- Id
,N'Igbo'                                            -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'74f981db-0453-4acc-b3af-359c2503f1ad'             -- Id
,N'English'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'7885864e-888d-4f7c-817e-0889f7c398fd'             -- Id
,N'Panjabi'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'79b1d886-b9e7-4d5f-b7be-343cfb44b769'             -- Id
,N'Nepali'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'7db7222a-3f9c-45c5-96aa-7e48ff184f5b'             -- Id
,N'Gujarati'                                        -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'85758549-52c3-4a2d-80d9-3e95dcc01a16'             -- Id
,N'Polish'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'8a7f49c3-fac1-4e61-87f1-3e8a91009316'             -- Id
,N'Sindhi'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'8f164323-68cf-413a-adef-4cf78ec17056'             -- Id
,N'Arabic'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'90ae9609-c23b-4880-89f1-982fc21c90d4'             -- Id
,N'Thai'                                            -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'93a0d0ee-ca98-4cc3-841e-8de700cd84d9'             -- Id
,N'Romanian'                                        -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'964684a4-1503-4bf9-b0ca-bc52f0c90891'             -- Id
,N'Assamese'                                        -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'969d3568-15e8-47c0-9521-4518b685ed61'             -- Id
,N'Burmese'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'972e2268-073a-4372-ac7d-00c49b8e3173'             -- Id
,N'Magahi'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'981a7655-7b0a-44b4-a552-b91c3613696e'             -- Id
,N'Zhuang'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'98c8cb09-c08c-4b09-8855-dd77391e173b'             -- Id
,N'Korean'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'9b4e006d-e1e3-4ed0-8c63-df2f69527330'             -- Id
,N'German'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'9f5fe7a7-0bf2-4204-94b2-7e6ce392aafd'             -- Id
,N'Azerbaijani'                                     -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'a03e009e-b488-4173-9a25-e72e8826e6e1'             -- Id
,N'Farsi'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'a1fd41a3-c474-48ca-b84b-e6cbaf5b1d47'             -- Id
,N'Sunda'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'a54a5a96-87d1-4c70-9f95-12f1a6ce74ee'             -- Id
,N'Serbo-croatian'                                  -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'aa788993-f7df-4035-9b1e-895dcf42ef8b'             -- Id
,N'Malay'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'aeded0b2-1b29-4d88-8368-4f9eaacc9707'             -- Id
,N'Dutch'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'b64fb0ed-6d22-44ed-bb62-bd0f01ffd0d0'             -- Id
,N'Swedish'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'b6b9aea1-6b5a-4554-8fbe-e6d35bd7180b'             -- Id
,N'Malagasy'                                        -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'b79686d8-e19b-4b61-aa42-5ec4fd0d7874'             -- Id
,N'Portuguese'                                      -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'b9f089d6-8501-46bb-83c7-ddfc0a435c78'             -- Id
,N'Kannada'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'bd0f627d-8f9f-4d6c-b77c-61ab83e01bd4'             -- Id
,N'Chinese'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'be5699be-6aca-4bab-8c1f-e1196ec82cf5'             -- Id
,N'Haryanvi'                                        -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'c3d0bc53-7d15-4f9c-b687-c569dbdc78aa'             -- Id
,N'Ukrainian'                                       -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'c4688611-c505-4d73-9819-5669d53d943c'             -- Id
,N'Napoletano-calabrese'                            -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'cebf428f-7718-47db-9bd8-ee616cbfab2b'             -- Id
,N'French'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'cf83b943-7715-4a5c-8b28-ca9255991a2a'             -- Id
,N'Greek'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'd07b1de6-fb6c-4040-b5e0-8849177791e3'             -- Id
,N'Belarusan'                                       -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'd559c7d7-b27e-4d88-9a5a-6ca45d690b5e'             -- Id
,N'Amharic'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'dbc2169c-ad5c-485b-b8a0-d4b53a99ec6d'             -- Id
,N'Malayalam'                                       -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'e6181c45-30a8-4319-be61-c1401b0d7f99'             -- Id
,N'Pashto'                                          -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'e73e6ea9-f21b-4723-b2cc-0ccc3418e49b'             -- Id
,N'Kurmanji'                                        -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'e96935e4-5f9c-45da-af4f-3e546c245f8a'             -- Id
,N'Urdu'                                            -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'ea1c857c-0b64-47cf-8037-b2fb934857c8'             -- Id
,N'Turkish'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'eb1e14c3-0066-4153-a29c-0d8c4fcffae9'             -- Id
,N'Marwari'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'eba349df-128a-4dcb-a483-d2501b26510a'             -- Id
,N'Cebuano'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'ecdab90e-d1d7-4575-80fd-dbdd4e53aa61'             -- Id
,N'Japanese'                                        -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'edd4d8e8-8f34-40f9-85ac-c9b42184a9ac'             -- Id
,N'Hausa'                                           -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'f307572f-818c-413b-ba31-cac0b1a7f57a'             -- Id
,N'Fulfulde'                                        -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'f6730792-912e-401b-94cc-59d48760044c'             -- Id
,N'Marathi'                                         -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'f6927bff-4299-4bfe-a362-ed74653a68f8'             -- Id
,N'Akan'                                            -- Name
)

INSERT [Languages] (
[Id],
[Name]
)
VALUES (
N'f6d2376b-36b5-42f3-a795-7787426b3bc4'             -- Id
,N'Bengali'                                         -- Name
)

GO
