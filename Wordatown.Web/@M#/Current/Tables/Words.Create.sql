-- Words Table ========================
CREATE TABLE [Words] (
    [Id] uniqueidentifier PRIMARY KEY NONCLUSTERED,
    [Rank] int  NOT NULL,
    [Text] nvarchar(50)  NOT NULL,
    [IsPhrase] bit  NOT NULL
)
CREATE INDEX [IX_Words->Rank] ON [Words] ([Rank])

GO

