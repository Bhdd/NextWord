﻿SET DATEFORMAT dmy
GO

INSERT [EmailTemplates] (
[Id],
[Body],
[Key],
[MandatoryPlaceholders],
[Subject]
)
VALUES (
N'337a69a8-b057-492d-b92c-4cc29c57bfed'             -- Id
,N'<p>Dear [#USERID#],</p>

<p>Please click on the following link to reset your password. If you did not request this password reset then please contact us.</p>

<div>  [#LINK#] </div>

<p><br />
Best regards,</p>

<p><br />
Regards</p>' -- Body
,N'RecoverPassword'                                 -- Key
,N'USERID, LINK'                                    -- MandatoryPlaceholders
,N'Recover Password'                                -- Subject
)

INSERT [EmailTemplates] (
[Id],
[Body],
[Key],
[MandatoryPlaceholders],
[Subject]
)
VALUES (
N'752f5b25-ddca-45fc-aad7-0b0b12ec3dc0'             -- Id
,N'[#EMAIL#]
[#PIN#]
'                            -- Body
,N'UserRegistration'                                -- Key
,N'EMAIL, PIN'                                      -- MandatoryPlaceholders
,N'WordaTown | Your PIN to Login'                   -- Subject
)

GO
