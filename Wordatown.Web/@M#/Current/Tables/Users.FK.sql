ALTER TABLE [Users] ADD Constraint
                [FK_User.NativeLanguage->Language]
                FOREIGN KEY ([NativeLanguage])
                REFERENCES [Languages] ([ID])
                ON DELETE NO ACTION 
GO