ALTER TABLE [Learnings] ADD Constraint
                [FK_UserLearn.Word]
                FOREIGN KEY ([Word])
                REFERENCES [Words] ([ID])
                ON DELETE NO ACTION 
GO
ALTER TABLE [Learnings] ADD Constraint
                [FK_Learning.User]
                FOREIGN KEY ([User])
                REFERENCES [Users] ([ID])
                ON DELETE NO ACTION 
GO