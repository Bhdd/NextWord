﻿SET DATEFORMAT dmy
GO

INSERT [Administrators] (
[Id],
[ImpersonationToken]
)
VALUES (
N'0734677a-c81a-4d15-a679-f8c7eb8d6047'             -- Id
,NULL                                               -- ImpersonationToken
)

INSERT [Administrators] (
[Id],
[ImpersonationToken]
)
VALUES (
N'3536b2da-2e97-4627-bfc0-9036527b016b'             -- Id
,N''                                                -- ImpersonationToken
)

INSERT [Administrators] (
[Id],
[ImpersonationToken]
)
VALUES (
N'89001798-6ddd-46f3-9e94-44f3695820eb'             -- Id
,N''                                                -- ImpersonationToken
)

GO
