-- UserAssessments Table ========================
CREATE TABLE [UserAssessments] (
    [Id] uniqueidentifier PRIMARY KEY NONCLUSTERED,
    [User] uniqueidentifier  NOT NULL,
    [Word] nvarchar(200)  NOT NULL,
    [Known] bit  NULL
)

