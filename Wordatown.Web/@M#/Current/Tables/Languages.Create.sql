-- Languages Table ========================
CREATE TABLE [Languages] (
    [Id] uniqueidentifier PRIMARY KEY NONCLUSTERED,
    [Name] nvarchar(50)  NOT NULL
)

