ALTER TABLE [UserAssessments] ADD Constraint
                [FK_UserAssessment.User]
                FOREIGN KEY ([User])
                REFERENCES [Users] ([ID])
                ON DELETE NO ACTION 
GO