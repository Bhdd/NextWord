var v105 =
{
    Name: "Contacts List",
    Entity: "Contact",
    RenderMode: "Grid",
    RootCssClass: "standard-list",
    DefaultPagingSize: "10",
    LazyLoad: true,
    ModuleHeader: "This module allows you to see all your contacts, interact with them and generally have a lot of fun with it. Am I long enough now?",
    ModuleHeaderText: "Contacts",
    PullToRefresh: true,
    ShowHeaderRow: true,
    Sortable: true,
    StartUpBehaviour: "Load list",
    WrapListItemTemplate: true,
    
    /* ================================= Elements ================================= */
    Elements:
    [
        /* -------------------------- [[ [Contacts List].Photo ]] ----------------- */
        {
            Type: "ListButton",
            Name: "Photo",
            Text: "Photo",
            CausesValidation: true,
            CssClass: "icon",
            DisplayMode: "Default",
            GridColumnCssClass: "actions",
            ImageUrl: "c#:Item.Photo.Path",
            LabelText: "Actions",
            ColumnVisibility:
            {
                Rule: "Module.ShowPhotoColumn",
            },
            Visibility:
            {
                Rule: "Module.ShowPhotoColumn && Item.Photo.HasValue()",
            },
            Workflow:
            {
            },
        },
        /* -------------------------- [[ Name ]] ---------------------------------- */
        {
            Type: "ViewElement",
            Property: "Name",
            CssClass: "title",
            DisplayMode: "Default",
            IsSortable: true,
        },
        /* -------------------------- [[ Contact details ]] ----------------------- */
        {
            Type: "ViewElement",
            LabelText: "Contact details",
            ControlId: "ContactDetails",
            DisplayMarkup: "c#:new[]{Item.Email , Item.Tel}.Trim().ToString(\", \")",
            DisplayMode: "Default",
            IsSortable: true,
            NeedsMerging: true,
        },
        /* -------------------------- [[ [Contacts List].View ]] ------------------ */
        {
            Type: "ListButton",
            Name: "View",
            Text: "View",
            CausesValidation: true,
            CssClass: "view-row",
            DisplayMode: "Default",
            ImageUrl: "Images/Icons/Arrow-Right.png",
            Workflow:
            {
                Activities:
                [
                    /* Go Enter {send Item}......................... */
                    {
                        Type: "NavigateActivity",
                        Description: "Go Enter {send Item}",
                        Page: "Page 1 > Enter",
                        Target: "Same window",
                        SendReturnUrl: true,
                        
                        Parameters:
                        [
                            {
                                Key: "Item",
                                Type: "C#",
                                Value: "Item",
                            },
                        ],
                    },
                ],
            },
        },
        /* -------------------------- [[ [Contacts List].Delete ]] ---------------- */
        {
            Type: "ListButton",
            Name: "Delete",
            Text: "Delete",
            CausesValidation: true,
            ConfirmationText: "Are you sure you want to delete this contact?",
            CssClass: "delete-button",
            DisplayMode: "Default",
            GridColumnCssClass: "actions",
            LabelText: "Actions",
            Location: "Slide in",
            Workflow:
            {
                Activities:
                [
                    /* Delete item.................................. */
                    {
                        Type: "CommonActivity",
                        Description: "Delete item",
                        Action: "Delete item",
                    },
                    
                    /* Reload....................................... */
                    {
                        Type: "CommonActivity",
                        Description: "Reload",
                        Action: "Reload",
                    },
                ],
            },
        },
    ],
    
    /* ================================= Buttons ============================================ */
    Buttons:
    [
        /* ----------------------  << Add >>  ----------------------------------------------- */
        {
            Name: "Add",
            Text: "Add",
            CausesValidation: true,
            ImageUrl: "Images/Icons/White-Add.png",
            Location: "Nav bar right",
            Workflow:
            {
                Activities:
                [
                    /* Go Enter..................................... */
                    {
                        Type: "NavigateActivity",
                        Description: "Go Enter",
                        Page: "Page 1 > Enter",
                        Target: "Same window",
                        SendReturnUrl: true,
                    },
                ],
            },
        },
        
        /* ----------------------  << Reload >>  -------------------------------------------- */
        {
            Text: "Reload",
            CausesValidation: true,
            Location: "Top horizontal",
            Workflow:
            {
                Activities:
                [
                    /* Reload....................................... */
                    {
                        Type: "CommonActivity",
                        Description: "Reload",
                        Action: "Reload",
                    },
                ],
            },
        },
    ],
    
    Code:
    [
        
        /* ================================= CLASS CODE ========================================= */
        
        /* ---------------------- ShowPhotoColumn property ---------------------------------- */
        {
            Type: "ModuleProperty",
            Name: "ShowPhotoColumn",
            Title: "ShowPhotoColumn property",
            Event: "Class code",
            CustomPropertyType: "bool",
            Getter: "Items.Any(x=>x.Photo.HasValue())",
            CacheValue: true,
        },
    ],
}