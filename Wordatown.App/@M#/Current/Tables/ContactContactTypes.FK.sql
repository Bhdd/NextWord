ALTER TABLE [ContactContactTypes] ADD Constraint
                [FK_ContactContactType.Contact]
                FOREIGN KEY ([Contact])
                REFERENCES [Contacts] ([ID])
                ON DELETE NO ACTION 
GO
ALTER TABLE [ContactContactTypes] ADD Constraint
                [FK_ContactContactType.ContactType]
                FOREIGN KEY ([ContactType])
                REFERENCES [ContactTypes] ([ID])
                ON DELETE NO ACTION 
GO