ALTER TABLE [Contacts] ADD Constraint
                [FK_Contact.Type->ContactType]
                FOREIGN KEY ([Type])
                REFERENCES [ContactTypes] ([ID])
                ON DELETE NO ACTION 
GO