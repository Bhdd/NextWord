namespace UI.Pages
{
    using System.Threading.Tasks;
    using Zebble;

    partial class Words
    {
        public override async Task OnInitializing()
        {
            var isPhrase = Nav.Param<bool>("IsPhrase");
            if (!isPhrase) Data.Add("CurrentTab", "Words");
            else Data.Add("CurrentTab", "Phrases");

            await base.OnInitializing();

            PendingSegment.BorderRadius(topLeft: 20, bottomLeft: 20);
            ToLearnSegment.BorderRadius(topRight: 20, bottomRight: 20);
        }
    }
}