namespace UI.Pages
{
    using System.Threading.Tasks;
    using Domain;
    using Zebble;

    partial class Progress
    {
        int TotalKnownCount, TotalLearntCount;
        public override async Task OnInitializing()
        {
            var count = await Api.GetKnownCount();
            TotalKnownCount = count.KnownCount;
            TotalLearntCount = count.LearntCount;

            await base.OnInitializing();

            LearningSegment.BorderRadius(topLeft: 20, bottomLeft: 20);
            LearntSegment.BorderRadius(topRight: 20, bottomRight: 20);
        }
    }
}