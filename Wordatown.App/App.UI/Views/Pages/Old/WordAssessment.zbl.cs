﻿namespace UI.Pages
{
    using System.Threading.Tasks;

    partial class WordAssessmentPage
    {
        public override async Task OnInitializing()
        {
            await base.OnInitializing();
            NavBar.Title.Style.Width = new Zebble.Length.PercentageLengthRequest(100);
        }
    }
}