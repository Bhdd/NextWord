﻿namespace UI.Modules
{
    using System;
    using System.Threading.Tasks;
    using Zebble;
    using Domain;

    partial class ToLearnList
    {
        Word[] Items;
        bool IsPhrase = Nav.Param<bool>("IsPhrase");

        public override async Task OnInitializing()
        {
            Items = await Api.GetToLearnWords(isPhrase: IsPhrase);
            await base.OnInitializing();

            Page.PulledToRefresh.Handle(Reload);
        }

        partial class Row
        {
            KnownList Module => FindParent<KnownList>();

            Task RowTapped() => Nav.Forward<Pages.Definition>(new { Word = Item.Text });
        }

        async Task Reload() => await List.UpdateSource(Items = await Api.GetToLearnWords(isPhrase: IsPhrase));
    }
}