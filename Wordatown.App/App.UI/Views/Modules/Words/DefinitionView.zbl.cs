﻿namespace UI.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Zebble;
    using Domain;

    partial class DefinitionView
    {
        bool ShowNext = Nav.Param<bool>("ShowNext");
        string Word = Nav.Param<string>("Word");
        string Meaning = "";
        string Translation = "";

        public override async Task OnInitializing()
        {
            var translationResult = await Api.GetMeaning(Word);
            Meaning = translationResult.Meaning;
            Translation = translationResult.Translation;

            await base.OnInitializing();
        }

        public async Task PronounceButtonTapped()
        {
            try
            {
                await Device.Speech.Speak(Word, new Device.SpeechSettings
                {
                    Pitch = 1.2F,
                    Speed = 1.5F,
                    Volume = 0.8F,
                    Language = new Device.SpeechLanguage("en")
                });
            }
            catch (Exception ex)
            {
                await Alert.Toast(ex.Message);
            }
        }

        public async Task NextButtonTapped()
        {

        }
    }
}