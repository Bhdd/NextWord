﻿namespace UI.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Zebble;
    using Domain;

    partial class PendingList
    {
        List<Word> Items;
        bool IsPhrase = Nav.Param<bool>("IsPhrase");

        public override async Task OnInitializing()
        {
            Items = await Api.GetPendingWords(isPhrase: IsPhrase);
            await base.OnInitializing();
        }

        public async Task ShortcutButtonTapped()
        {
            ///TODO: Change to overlay when plugin is ready
            await Alert.Show("Learn a shortcut!",

                @"Step 1: Jump to any word down the list where you're confident you know 100% of the words before that.

Step 2: Tap the word and hold for a second.

Step 3: Tap ""I know all words before this"""
                );
        }

        partial class Row
        {
            PendingList Module => FindParent<PendingList>();

            public async Task KnownButtonTapped()
            {
                if (CssClass == "known") await SetCssClass("");
                else await SetCssClass("known");
                await Api.ToggleWordAsKnown(Item);
            }

            public async Task ToLearnButtonTapped()
            {
                if (CssClass == "tolearn") await SetCssClass("");
                else await SetCssClass("tolearn");
                await Api.ToggleWordAsToLearn(Item);
            }

            async Task Longpressed()
            {
                var rank = Item.Rank;
                await SetCssClass("longpressed");

                var choice = await Alert.Show("Do you know all words before this?", "", KeyValuePair.Of("Yes!", true), KeyValuePair.Of("Forget it", false));
                if (choice)
                {
                    await Api.MarkWordsBeforeAsKnown(rank, Module.IsPhrase);
                    await Alert.Toast("We've saved your known words.");
                    await Nav.Reload();
                }
            }
        }
    }
}