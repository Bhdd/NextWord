namespace UI.Modules
{
    using System;
    using System.Threading.Tasks;
    using Domain;
    using Zebble;

    public partial class LearnForm
    {
        public static Learning[] WordsQueue;
        public static int CurrentIndex = 0;

        public bool QueueNotEmpty => WordsQueue != null && WordsQueue.Length > 0;

        public override async Task OnInitializing()
        {
            WordsQueue = await Api.GetWordsInLearningQueue();

            await base.OnInitializing();

            //border-radius: 50%
            WordCircle.BorderRadius(all: (Root.ActualWidth * 0.7f) / 2);
            SkipButton.BorderRadius(all: (Root.ActualWidth * 0.2f) / 2);
            NotSureButton.BorderRadius(all: (Root.ActualWidth * 0.2f) / 2);
        }

        public override async Task OnInitialized()
        {
            await base.OnInitialized();

            if (QueueNotEmpty)
            {
                if (CurrentIndex >= WordsQueue.Length) CurrentIndex = 0;
                Word.Text = WordsQueue[CurrentIndex].Word.Text.ToProperCase();
            }
        }

        static async Task NotSureButtonTapped()
        {
            if (WordsQueue.Length > 0)
            {
                if (CurrentIndex >= WordsQueue.Length) CurrentIndex = 0;
                var userLearn = WordsQueue[CurrentIndex];
                CurrentIndex += 1;

                if (await Api.MarkWordAsNotSure(userLearn.ID))
                    await Nav.Forward<Pages.Definition>(new
                    {
                        Word = userLearn.Word.Text,
                        ShowNext = true
                    });
            }
        }

        async Task SkipButtonTapped()
        {
            if (CurrentIndex >= WordsQueue.Length) CurrentIndex = 0;
            var userlearn = WordsQueue[CurrentIndex];
            CurrentIndex += 1;

            await Api.SkipWord(userlearn.ID);

            if (CurrentIndex < WordsQueue.Length) Word.Text = WordsQueue[CurrentIndex].Word.Text.ToProperCase();
            else await Nav.Reload();
        }
    }
}
