namespace UI.Modules
{
    using System;
    using System.Threading.Tasks;
    using Domain;
    using Zebble;

    public partial class SearchForm
    {
        Word[] Items;
        string SearchTerm = Nav.Param<string>("SearchTerm");

        public override async Task OnInitializing()
        {
            if (SearchTerm.HasValue()) Items = await Api.SearchWords(SearchTerm);
            await base.OnInitializing();
        }

        async Task ApplySearch()
        {
            await Waiting.Show();
            Items = await Api.SearchWords(SearchInput.Text);
            await Waiting.Hide();
            await List.UpdateSource(Items);
        }

        partial class Row
        {
            SearchForm Module => FindParent<SearchForm>();

            public Task RowTapped() => Nav.Go<Pages.Definition>(transition: PageTransition.DropUp, navParams: new { Word = Item.Text });
        }
    }
}
