﻿namespace UI.Modules
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Domain;
    using Zebble;

    partial class LoginModule
    {
        string Email;
        Language Lang;
        public override async Task OnInitializing()
        {
            await base.OnInitializing();
            Email = Nav.Param<string>("UserEmail");
            Lang = Nav.Param<Language>("Lang");
        }

        public async Task LoginButtonTapped()
        {
            if (Pin.Text.HasValue())
            {
                await Authentication.Login(Email, Pin.Text, Lang);
                var token = BaseApi.GetSessionToken();
                if (token.HasValue()) await Nav.Go<Pages.Learn>(PageTransition.SlideForward);
            }
            else await Alert.Show("Please enter the pin.");
        }
    }
}