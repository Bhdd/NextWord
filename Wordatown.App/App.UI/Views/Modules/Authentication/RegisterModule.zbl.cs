﻿namespace UI.Modules
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Domain;
    using Zebble;

    partial class RegisterModule
    {
        Language[] Languages;
        public override async Task OnInitializing()
        {
            Languages = await Api.GetLanguages();
            await base.OnInitializing();
        }

        async Task NextButtonTapped()
        {
            if (Email.Text.LacksValue() || !Email.Text.IsValidEmailAddress())
            {
                await Alert.Show("Please enter a valid email address.");
                return;
            }

            var nativeLanguage = Language.Value as Language;

            if (nativeLanguage == null)
            {
                await Alert.Show("Please choose your native language.");
                return;
            }

            if (await Authentication.Register(Email.Text, nativeLanguage))
                await Nav.Forward<Pages.Login>(new { UserEmail = Email.Text, Lang = nativeLanguage });

        }
    }
}