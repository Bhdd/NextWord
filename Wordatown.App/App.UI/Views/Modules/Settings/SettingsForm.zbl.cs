﻿namespace UI.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Zebble;
    using Domain;
    using System.Linq;

    partial class SettingsForm
    {
        Language[] Languages;
        Reminder[] Reminders;

        public override async Task OnInitializing()
        {
            Languages = await Api.GetLanguages();
            Reminders = await Api.GetReminders();

            await base.OnInitializing();

            await (Nav.CurrentPage as NavBarPage).GetNavBar().AddButton(ButtonLocation.Right, CreateSaveButton());

            LanguageFormField.Value = User.Current.NativeLanguage;

            foreach (var reminder in Reminders)
                await RemindersContainer.Add(new ReminderInput { DefaultValue = reminder.Time });
        }

        Task AddButtonTapped() => RemindersContainer.Add(new ReminderInput());

        async Task SaveButtonTapped()
        {
            var reminders = RemindersContainer.CurrentChildren<ReminderInput>().Where(x => x.SelectedValue.HasValue).Select(x => x.SelectedValue.Value);

            var nativeLanguage = LanguageFormField.Value as Language;

            var saved = await Api.SaveSettings(reminders, nativeLanguage);

            if (saved)
            {
                User.Current.NativeLanguage = nativeLanguage;
                await Alert.Toast("Your setting has been saved successfully.");
            }
        }

        public ImageView CreateSaveButton()
        {
            var saveButton = new ImageView { CssClass = "save", Id = "SaveSettingsButton", };
            saveButton.Tapped.Handle(SaveButtonTapped);
            return saveButton;
        }
    }
}