﻿namespace UI.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Zebble;
    using Domain;

    partial class ReminderInput
    {
        public TimeSpan? DefaultValue;
        public TimeSpan? SelectedValue => (TimeSpan)TimePickerFormField?.Value;

        public override async Task OnInitializing()
        {
            await base.OnInitializing();
            TimePickerFormField.Value = DefaultValue;
        }

        Task RemoveButtonTapped() => this.RemoveSelf();
    }
}