﻿namespace UI.Modules
{
    using System.Threading.Tasks;
    using Zebble;
    using Domain;

    partial class LearntList
    {
        Learning[] Items;

        public override async Task OnInitializing()
        {
            Items = await Api.GetLearntWords();

            await base.OnInitializing();
        }

        partial class Row
        {
            LearntList Module => FindParent<LearntList>();

            Task RowTapped() => Nav.Go<Pages.Definition>(new { Word = Item.Word.Text });
        }
    }
}