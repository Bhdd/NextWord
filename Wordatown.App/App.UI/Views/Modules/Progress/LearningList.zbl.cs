﻿namespace UI.Modules
{
    using System.Threading.Tasks;
    using Zebble;
    using Domain;

    partial class LearningList
    {
        Learning[] Items;

        public override async Task OnInitializing()
        {
            Items = await Api.GetLearningWords();

            await base.OnInitializing();
        }

        partial class Row
        {
            LearningList Module => FindParent<LearningList>();

            Task RowTapped() => Nav.Go<Pages.Definition>(new { Word = Item.Word.Text });
        }
    }
}