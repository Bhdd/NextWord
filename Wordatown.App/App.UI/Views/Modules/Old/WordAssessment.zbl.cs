﻿namespace UI.Modules
{
    using System;
    using System.Threading.Tasks;
    using Domain;
    using Zebble;

    partial class WordAssessment
    {
        static int from;
        static int to;
        static string FromToString;
        static string PickedString1;
        static string PickedString2;
        static string PickedEstimateString;
        static string[] Items;
        static int page = 1;
        public override async Task OnInitializing()
        {
            var page = Nav.Param<int?>("page");
            var assessment = Nav.Param<int?>("assess");
            if (assessment.HasValue && assessment == 1)
            {
                await Api.RemoveAll();
            }
            if (page == null)
            {
                page = 1;
            }
            from = (page.Value - 1) * 1000;
            if (from == 0) from = 1;
            to = page.Value * 1000;
            FromToString = "Word range: " + from.ToString() + " to " + to.ToString();
            PickedString1 = "We have picked 20 words randomly from the range of " + from + " to " + to + "th most frequently used words.";
            PickedString2 = "Depending on how many you know, we can then adjust your learning process to make best use of your time.";
            PickedEstimateString = "Estimate your knowledge of the words in the  " + from + " to " + to + "  range.";
            var selected = Nav.Param<string>("selected");
            if (selected.HasValue())
            {
                Items[Items.ToLower().IndexOf(selected.ToLower())] = string.Empty;
            }
            else
                Items = await Api.GetUserAssessmentList(page.Value);
            await base.OnInitializing();
            Page.PulledToRefresh.Handle(Reload);
        }

        partial class Row
        {
            #region TODO: Need access to the list container module?
            #endregion

            public override async Task OnInitializing()
            {
                if (Item.Length > 0)
                    Item = Item.ToProperCase();

                await base.OnInitializing();
                if (Item == string.Empty) this.Ignored = true;
            }

            async Task YesButtonTapped()
            {
                await Api.ToggleKnown(Item, true);
                await Nav.Go<Pages.WordAssessmentPage>(new { page = Nav.Param<int?>("page"), selected = Item }, PageTransition.None);
            }
            async Task NoButtonTapped()
            {
                await Api.ToggleKnown(Item, false);
                await Nav.Go<Pages.WordAssessmentPage>(new { page = Nav.Param<int?>("page"), selected = Item }, PageTransition.None);
            }
            async Task NotSureButtonTapped()
            {
                await Api.ToggleKnown(Item, null);
                await Nav.Go<Pages.WordAssessmentPage>(new { page = Nav.Param<int?>("page"), selected = Item }, PageTransition.None);
            }
        }
        async Task Reload() => await MyGrid.UpdateSource(Items = await GetSource());

        static async Task<string[]> GetSource()
        {
            Items = await Api.GetUserAssessmentList(page);
            return Items;
        }
        async Task NextButtonTapped()
        {
            var pg = Nav.Param<int?>("page");
            if (pg == null) pg = 1;
            pg = pg + 1;
            if (pg > 12)
            {
                await Alert.Show("The Assessment Completed!");
                await Nav.Go<Pages.Statistics>(PageTransition.None);
            }
            else
                await Nav.Forward<Pages.WordAssessmentPage>(new { page = pg });
        }
    }
}