﻿namespace UI.Modules
{
    using System;
    using System.Threading.Tasks;
    using Domain;
    using Zebble;

    partial class GapMap
    {
        const int BarHeight = 45;
        const int rulerHeight = 20;
        public override async Task OnInitializing()
        {
            int[,] stat = await Api.GetGapMapData();
            await base.OnInitializing();
            for (int i = 0; i < 12; i++)
            {
                var rowNo = new TextView((i + 1) + "K");
                rowNo.Style.Height = BarHeight;
                rowNo.Style.Font.Size = 14;
                var head = new Stack();
                head.Style.Height = BarHeight;
                head.Width.AsText = "6%";
                await head.Add(rowNo);

                var canvasNA = new Canvas();
                canvasNA.Width.AsText = (Math.Round((float)100 * stat[i, 0] / 1000)) + "%";
                canvasNA.Style.Height = BarHeight;
                canvasNA.Style.BackgroundColor = Colors.LightGray;

                var canvasKnown = new Canvas();
                canvasKnown.Width.AsText = (Math.Round((float)100 * stat[i, 1] / 1000)) + "%";
                canvasKnown.Style.Height = BarHeight;
                canvasKnown.Style.BackgroundColor = Colors.GreenYellow;
                var canvasUnKnown = new Canvas();
                canvasUnKnown.Width.AsText = (Math.Round((float)100 * stat[i, 2] / 1000)) + "%";
                canvasUnKnown.Style.Height = BarHeight;
                canvasUnKnown.Style.BackgroundColor = Colors.Yellow;

                var stackRow = new Stack() { Direction = RepeatDirection.Horizontal, HorizontalAlignment = HorizontalAlignment.Left };
                stackRow.Width.Set(Zebble.Length.AutoStartegy.Container);
                stackRow.Height.Set(BarHeight);
                await stackRow.Add(canvasNA);
                await stackRow.Add(canvasKnown);
                await stackRow.Add(canvasUnKnown);
                var stackWholeRow = new Stack() { HorizontalAlignment = HorizontalAlignment.Left, Direction = RepeatDirection.Horizontal };
                stackWholeRow.Height.AsText = "100%";
                stackWholeRow.Style.Height = BarHeight;
                stackWholeRow.Padding(top: 5, right: 5);
                await stackWholeRow.Add(head);
                await stackWholeRow.Add(stackRow);
                await StackChart.Add(stackWholeRow);
            }
            var measureHead = new Stack();
            measureHead.Style.Height = rulerHeight;
            measureHead.Width.AsText = "6%";
            measureHead.Id = "MeasureHead";
            await StackMeasure.Add(measureHead);
            for (int j = 0; j <= 90; j += 10)
            {
                var tv = new TextView(j + "%");
                tv.Width.AsText = "9.4%";
                tv.Style.Height = rulerHeight;
                tv.TextAlignment = Alignment.Left;
                await StackMeasure.Add(tv);
            }
        }

    }
}