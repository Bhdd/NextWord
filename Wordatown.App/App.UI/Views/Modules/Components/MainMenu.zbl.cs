namespace UI.Modules
{
    using System.Threading.Tasks;
    using UI.Templates;
    using Zebble;

    partial class MainMenu
    {
        public override async Task OnInitializing()
        {
            await base.OnInitializing();
            Circle.BorderRadius(View.Root.ActualWidth);
            ClipChildren = true;
        }

        public async Task LogoutTapped()
        {
            Nav.DisposeCache();
            await Domain.Api.DisposeCache();
            if (Device.IO.File("SessionToken.txt").Exists)
            {
                Device.IO.File("SessionToken.txt").Delete();
                await Nav.Go<Pages.Register>(PageTransition.None);
            }
        }

        public async Task CloseTapped() => await MenuDisplayer.Current.HideMenu();
        public async Task ShareMeTapped() => await Nav.Go<Pages.Progress>(PageTransition.Fade);
        public async Task FeedbackTapped() => await Nav.Go<Pages.Progress>(PageTransition.Fade);
        public async Task AboutUsTapped() => await Nav.Go<Pages.Progress>(PageTransition.Fade);
        public async Task ContactUsTapped() => await Nav.Go<Pages.Progress>(PageTransition.Fade);

    }
}