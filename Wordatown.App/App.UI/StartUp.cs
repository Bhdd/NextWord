namespace UI
{
    using System;
    using System.Threading.Tasks;
    using Domain;
    using Zebble;

    public partial class StartUp : Zebble.StartUp
    {
        public override async Task Run()
        {
            ApplicationName = "Wordatown";

            await InstallIfNeeded();

            CssStyles.LoadAll();

            Device.ThreadPool.Run(() => LoadFirstPage()).RunInParallel();
        }

        public static async Task LoadFirstPage()
        {
            var sessionToken = BaseApi.GetSessionToken();

            if (sessionToken.HasValue())
            {
                User.Current = await Authentication.GetUser();
                await Nav.Go<Pages.Learn>();
            }
            else await Nav.Go<Pages.Register>();
        }
    }
}