﻿namespace UI.Templates
{
    using System.Threading.Tasks;
    using Modules;
    using Zebble;

    public class MenuDisplayer : MainMenuLauncher<MainMenu>
    {
        public override async Task OnInitializing()
        {
            MenuWidth = View.Root.ActualWidth;
            await base.OnInitializing();
        }
    }
}