namespace UI.Templates
{
    using Zebble;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using System.Linq;

    public class Blank : Page
    {
        public ScrollView BodyScroller = new ScrollView().Id("BodyScroller");
        public Stack Body;
        public Canvas TopLeft, TopRight, BottomLeft, BottomRight;

        protected override async Task InitializeFromMarkup()
        {
            await base.InitializeFromMarkup();

            await Add(BodyScroller);
            await BodyScroller.Add(Body = new Stack().Id("Body"));

            await BodyScroller.Add(TopLeft = new Canvas().Id("TopLeft").BorderRadius(Root.ActualWidth / 5));
            await BodyScroller.Add(TopRight = new Canvas().Id("TopRight").BorderRadius(Root.ActualWidth / 3));
            await Add(BottomLeft = new Canvas().Id("BottomLeft").BorderRadius(Root.ActualWidth / 4));
            await Add(BottomRight = new Canvas().Id("BottomRight").BorderRadius(Root.ActualWidth / 6));
        }
    }
}