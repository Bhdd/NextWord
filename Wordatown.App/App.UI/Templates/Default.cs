namespace UI.Templates
{
    using System;
    using System.Threading.Tasks;
    using Zebble;
    using System.Linq;

    public class Default : NavBarTabsPage<Modules.MainTabs>
    {
        public static MenuDisplayer MenuDisplayer;
        public override async Task OnInitializing()
        {
            await base.OnInitializing();
            await WhenShown(() => new MenuDisplayer().Setup());
        }

        protected override Task AddBackOrMenu() => AddNavBarButtons();

        async Task AddNavBarButtons()
        {
            var menu = CreateMenuIcon();
            menu.On(x => x.Tapped, async () => await menu.Flash().ContinueWith(t => OnMenuTapped()));
            await NavBar.AddButton(ButtonLocation.Left, menu);

            if (!(Nav.CurrentPage is Pages.Search || Nav.CurrentPage is Pages.Settings))
            {
                var searchButton = CreateSearchButton();
                searchButton.On(x => x.Tapped, async () => await searchButton.Flash().ContinueWith(t => Nav.Forward<Pages.Search>(PageTransition.DropDown)));
                await NavBar.AddButton(ButtonLocation.Right, searchButton);
            }
        }

        protected override bool ShowTabsAtTheTop() => false;

        protected override View CreateMenuIcon() => new ImageView { CssClass = "menu", Id = "MenuButton" };

        View CreateSearchButton() => new ImageView { CssClass = "search", Id = "SerachButton" };

        protected override Task OnMenuTapped() => MenuDisplayer.Current.Show();

    }
}