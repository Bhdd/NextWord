namespace UI.Templates
{
    using System.Threading.Tasks;
    using Zebble;

    [PopupPage]
    public class Modal : Page
    {
        public Stack Body;

        public override async Task OnInitializing()
        {
            await base.OnInitializing();
            await Add(Body = new Stack());
        }

        public override async Task OnPreRender()
        {
            await base.OnPreRender();
            Margin.Top.BindTo(Root.Height, Body.Height, (r, b) => (r - b) / 2);
        }

        public string Title { get { return string.Empty; } set { } }
    }
}