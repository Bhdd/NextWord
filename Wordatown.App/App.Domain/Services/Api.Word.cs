﻿namespace Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Zebble;

    partial class Api
    {
        public static Task<Learning[]> GetWordsInLearningQueue()
        {
            return Get<Learning[]>("api/v1/words/learningqueue",
                errorAction: OnError.Alert,
                cacheChoice: ApiResponseCache.Refuse);
        }

        public static Task<bool> MarkWordAsNotSure(Guid userLearnId) => Put($"api/v1/learning/{userLearnId}/notsure");

        public static Task<bool> SkipWord(Guid userLearnId) => Put($"/api/v1/learning/{userLearnId}/skip");

        public static Task<List<Word>> GetPendingWords(bool isPhrase)
        {
            return Get<List<Word>>($"/api/v1/words/pending/{isPhrase}",
                errorAction: OnError.Toast,
                cacheChoice: ApiResponseCache.AcceptButWarn);
        }

        public static Task<bool> ToggleWordAsKnown(Word word) => Put($"/api/v1/learning/{word.ID}/known", showWaiting: false);

        public static Task<bool> ToggleWordAsToLearn(Word word) => Put($"/api/v1/learning/{word.ID}/tolearn/", showWaiting: false);

        public static Task<bool> MarkWordsBeforeAsKnown(int rank, bool isPhrase)
            => Put($"/api/v1/learning/{rank}/markwordsbeforeasknown/{isPhrase}", showWaiting: true);

        public static Task<Word[]> GetKnownWords(bool isPhrase)
        {
            return Get<Word[]>($"api/v1/Words/known/{isPhrase}",
                errorAction: OnError.Toast,
                cacheChoice: ApiResponseCache.AcceptButWarn);
        }

        public static Task<Word[]> GetToLearnWords(bool isPhrase)
        {
            return Get<Word[]>($"api/v1/Words/tolearn/{isPhrase}",
               errorAction: OnError.Toast,
               cacheChoice: ApiResponseCache.AcceptButWarn);
        }

        public class WordCount { public int KnownCount, LearntCount; }
        public static Task<WordCount> GetKnownCount()
        {
            return Get<WordCount>($"api/v1/Words/knowncount/",
               errorAction: OnError.Toast,
               cacheChoice: ApiResponseCache.AcceptButWarn);
        }

        public static async Task<Learning[]> GetLearntWords()
        {
            return await Get<Learning[]>($"api/v1/words/learnt/",
               errorAction: OnError.Toast,
               cacheChoice: ApiResponseCache.AcceptButWarn);
        }

        public static Task<Learning[]> GetLearningWords()
        {
            return Get<Learning[]>($"api/v1/words/learning/",
               errorAction: OnError.Toast,
               cacheChoice: ApiResponseCache.AcceptButWarn);
        }

        public class WordMeaning { public string Meaning, Translation; }
        public static Task<WordMeaning> GetMeaning(string word)
        {
            return Get<WordMeaning>($"/api/v1/words/{word}/meaning",
               errorAction: OnError.Toast,
               cacheChoice: ApiResponseCache.Accept);
        }

        public static Task<Word[]> SearchWords(string word) => Get<Word[]>($"api/v1/words/{word}/search");

        //Doomed
        public static Task<string[]> GetUserAssessmentList(int page)
        {
            return Api.Get<string[]>($"api/UserAssessment/GetUserAssessmentList?page=" + page, null, OnError.Alert, ApiResponseCache.Refuse);
        }

        public static Task RemoveAll() => Api.Delete($"api/UserAssessment/RemoveAll", null, OnError.Alert);

        public static Task<string[]> ToggleKnown(string word, bool? known)
        {
            return Api.Put<string[]>($"api/UserAssessment/ToggleKnown?word=" + word + "&known=" + known ?? "null", null, OnError.Alert);
        }

        public static Task<int[,]> GetWordsStatistics()
        {
            return Api.Get<int[,]>($"/api/words/GetWordsStatistics", null, OnError.Alert);
        }

        public static Task<int[,]> GetGapMapData()
        {
            return Api.Get<int[,]>($"/api/UserAssessment/GetGapMapData", null, OnError.Alert);
        }
    }
}