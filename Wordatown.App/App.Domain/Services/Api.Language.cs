﻿using System;
using System.Threading.Tasks;
using Zebble;

namespace Domain
{
    partial class Api : BaseApi
    {
        public static Task<Language[]> GetLanguages() => Get<Language[]>("/api/v1/Language",
            cacheChoice: ApiResponseCache.Prefer,
            errorAction: OnError.Alert);

        public static Task<bool> UpdateLanguage(Guid id) => Put("api/Language/UpdateLanguage?ID=" + id, null, OnError.Alert);
    }
}
