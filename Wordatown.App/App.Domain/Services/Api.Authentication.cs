namespace Domain
{
    using System;
    using System.Threading.Tasks;
    using Zebble;

    public partial class Authentication : BaseApi
    {
        public static async Task<bool> Login(string email, string password, Language language)
        {
            var token = await Post<string>("api/v1/authentication/login", new
            {
                Email = email,
                Password = password,
                languageId = language.ID,
                DeviceModel = Device.OS.HardwareModel,
                DeviceType = User.DevicePlatform
            });

            if (token.LacksValue()) return false;

            await SaveCurrentUser(token);

            return true;
        }

        static async Task SaveCurrentUser(string token)
        {
            Device.IO.File("SessionToken.txt").WriteAllText(token);

            User.Current = await GetUser();
        }

        public static Task<User> GetUser()
        {
            return Get<User>("api/v1/authentication/user");
        }

        public static async Task<bool> Register(string email, Language language)
        {
            return await Post<bool>("api/v1/authentication/register", new
            {
                Email = email,
                LanguageId = language.ID,
                DeviceModel = Device.OS.HardwareModel,
                DeviceType = User.DevicePlatform
            });
        }
    }
}