﻿namespace Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Zebble;

    partial class Api
    {
        public static Task<Reminder[]> GetReminders()
        {
            return Get<Reminder[]>($"api/v1/learning/reminders",
                errorAction: OnError.Toast,
                cacheChoice: ApiResponseCache.Refuse);
        }

        public static Task<bool> SaveSettings(IEnumerable<TimeSpan> reminders, Language language)
        {
            return Post<bool>($"/api/v1/learning/settings/",
                new { Reminders = reminders, LanguageId = language.ID }, showWaiting: true);
        }
    }
}
