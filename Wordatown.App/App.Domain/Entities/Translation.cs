﻿using System;

namespace Domain
{
    public partial class Translation
    {
        public Guid ID { get; set; }

        public string Language { get; set; }

        public string TranslationText { get; set; }

        public string Word { get; set; }

        public override string ToString() => TranslationText;
    }
}