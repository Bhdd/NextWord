﻿namespace Domain
{
    using System;
    using Zebble.Data;

    public partial class Learning : GuidEntity
    {
        public int TotalIsKnowns { get; set; }
        public string DateLastShownDifference { get; set; }
        public DateTime? DateAdded { get; set; }
        public int Delay { get; set; } = 1;
        public bool? IsKnown { get; set; }
        public bool IsLearnt { get; }
        public DateTime? DateLastShown { get; set; }
        public DateTime? DateToBeShown { get; set; }
        public int TotalNotSures { get; set; }
        public int? TotalRepeats { get; set; }
        public int TotalSkips { get; set; }
        public Word Word { get; set; }
    }
}