﻿namespace Domain
{
    using System;
    public partial class UserAssessment
    {

        public bool? Known { get; set; }

        public string Word { get; set; }

        public Guid? UserId { get; set; }

        public User User
        {
            get; set;
        }
    }
}