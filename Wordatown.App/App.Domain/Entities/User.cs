﻿using System;
using Zebble;

namespace Domain
{
    public partial class User
    {
        public static User Current;
        public static string DevicePlatform => Device.Platform.ToString() == "IOS" ? "iOS" : Device.Platform.ToString();
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Language NativeLanguage { get; set; }

        public string Name => FirstName + " " + LastName;

        public string Password { get; set; }

        #region Salt Property

        /// <summary>Gets or sets the value of Salt on this User instance.</summary>
        public string Salt { get; set; }
        public bool IsDeactivated { get; set; }
        #endregion

        #region Native language Association

        /// <summary>Gets or sets the ID of the associated Native language.</summary>
        public Guid? NativeLanguageId { get; set; }
        #endregion
    }
}