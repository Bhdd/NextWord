﻿namespace Domain
{
    using System;
    using System.Runtime.Serialization;
    using Zebble.Data;

    public class Language : GuidEntity
    {
        public string Name { get; set; }

        public override string ToString() => Name;
    }
}