﻿namespace Domain
{
    using System;

    public partial class Reminder
    {
        public Guid ID { get; set; }

        public TimeSpan Time { get; set; }
    }
}