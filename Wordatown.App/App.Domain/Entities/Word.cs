﻿namespace Domain
{
    using System;

    public partial class Word
    {
        public Guid ID { get; set; }
        public string Meaning { get; set; }
        public string Text { get; set; }
        public bool Type { get; set; }
        public int Rank { get; set; }
    }
}